import { env } from './config'
import { app } from './app'

app().then((server) =>
  server.listen(env.values.PORT, () => {
    console.info(`[Server] Up on port ${env.values.PORT}`)
  })
)
