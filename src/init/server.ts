import bodyParser from 'body-parser'
import express from 'express'
import fs from 'fs'
import http from 'http'
import https from 'https'
import { env } from '../config'
import { ErrorsController } from '../controllers'
import {
  CalendarsRoutes,
  EventsRoutes,
  FollowersRoutes,
  NotificationsRoutes,
  RSVPsRoutes,
  SessionsRoutes,
  UsersRoutes
} from '../routes'

export const init = (app: express.Application) => {
  const calendarsRoutes = CalendarsRoutes.getInstance()
  const eventsRoutes = EventsRoutes.getInstance()
  const errorsController = ErrorsController.getInstance()
  const followersRoutes = FollowersRoutes.getInstance()
  const rsvpsRoutes = RSVPsRoutes.getInstance()
  const notificationsRoutes = NotificationsRoutes.getInstance()
  const sessionsRoutes = SessionsRoutes.getInstance()
  const usersRoutes = UsersRoutes.getInstance()

  app.use(bodyParser.json())

  app.use('/api/users', usersRoutes.getRouter())
  app.use('/api/sessions', sessionsRoutes.getRouter())
  app.use('/api/notifications', notificationsRoutes.getRouter())
  app.use('/api/calendars', calendarsRoutes.getRouter())
  app.use('/api/followers', followersRoutes.getRouter())
  app.use('/api/events', eventsRoutes.getRouter())
  app.use('/api/rsvps', rsvpsRoutes.getRouter())
  app.use(errorsController.handle.bind(errorsController))

  const strategy = env.values.SSL_ENABLED ? initHTTPS : initHTTP

  return strategy(app)
}

const initHTTP = (app: express.Application) => {
  return http.createServer(app)
}

const initHTTPS = (app: express.Application) => {
  const key = fs.readFileSync('../ssl/bojescu.com.key')
  const cert = fs.readFileSync('../ssl/bojescu.com.crt')

  const creds: https.ServerOptions = {
    key,
    cert
  }

  return https.createServer(creds, app)
}
