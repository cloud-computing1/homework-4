import { CSRFRepository, SessionsRepository } from '../cache/repositories'
import {
  CalendarsRepository,
  EventsRepository,
  FollowersRepository,
  RSVPsRepository,
  UsersRepository
} from '../database/repositories'
import { FirebaseProxy } from '../proxy'
import {
  CalendarsService,
  CryptoService,
  CSRFService,
  ErrorsService,
  EventsService,
  FollowersService,
  NotificationsService,
  RSVPsService,
  SessionsService,
  UsersService,
  ValidationsService
} from '../services'

export const init = () => {
  const firebaseProxy = FirebaseProxy.getInstance()
  const sessionsRepository = SessionsRepository.getInstance()
  const csrfRepository = CSRFRepository.getInstance()

  const calendarsRepository = CalendarsRepository.getInstance()
  const eventsRepository = EventsRepository.getInstance()
  const followersRepository = FollowersRepository.getInstance()
  const rsvpsRepository = RSVPsRepository.getInstance()
  const usersRepository = UsersRepository.getInstance()

  const notificationsService = NotificationsService.getInstance(firebaseProxy)
  const rsvpsService = RSVPsService.getInstance(rsvpsRepository, notificationsService)
  const followersService = FollowersService.getInstance(followersRepository, notificationsService)
  const calendarsService = CalendarsService.getInstance(
    calendarsRepository,
    followersService,
    rsvpsService,
    notificationsService
  )
  const cryptoService = CryptoService.getInstance()
  CSRFService.getInstance(csrfRepository, cryptoService)
  EventsService.getInstance(eventsRepository, rsvpsService, notificationsService)
  SessionsService.getInstance(sessionsRepository, cryptoService)
  UsersService.getInstance(usersRepository, cryptoService, calendarsService)
  ErrorsService.getInstance()
  ValidationsService.getInstance()
}
