import {
  CalendarsController,
  EventsController,
  FollowersController,
  NotificationsController,
  RSVPsController,
  SessionsController,
  UsersController
} from '../controllers'
import {
  CalendarsRoutes,
  EventsRoutes,
  FollowersRoutes,
  NotificationsRoutes,
  RSVPsRoutes,
  SessionsRoutes,
  UsersRoutes
} from '../routes'

export const init = () => {
  const calendarsController = CalendarsController.getInstance()
  const eventsController = EventsController.getInstance()
  const followersController = FollowersController.getInstance()
  const rsvpsController = RSVPsController.getInstance()
  const sessionsController = SessionsController.getInstance()
  const usersController = UsersController.getInstance()
  const notificationsController = NotificationsController.getInstance()

  CalendarsRoutes.getInstance(sessionsController, calendarsController, eventsController, followersController)
  EventsRoutes.getInstance(sessionsController, eventsController)
  FollowersRoutes.getInstance(sessionsController, followersController)
  RSVPsRoutes.getInstance(sessionsController, rsvpsController)
  SessionsRoutes.getInstance(sessionsController)
  UsersRoutes.getInstance(sessionsController, usersController)
  NotificationsRoutes.getInstance(sessionsController, notificationsController)
}
