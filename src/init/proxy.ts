import { FirebaseProxy } from '../proxy'

export const init = () => {
  FirebaseProxy.getInstance()
}
