import process from 'process'

export const init = () => {
  process.on('uncaughtException', log)
  process.on('unhandledRejection', log)
}

export function log (err: Error) {
  console.error('[Error]', err.message, '\n', err.stack)
}
