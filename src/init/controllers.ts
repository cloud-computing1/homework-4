import {
  CalendarsController,
  ErrorsController,
  EventsController,
  FollowersController,
  NotificationsController,
  RSVPsController,
  SessionsController,
  UsersController
} from '../controllers'
import {
  CalendarsService,
  CSRFService,
  ErrorsService,
  EventsService,
  FollowersService,
  NotificationsService,
  RSVPsService,
  SessionsService,
  UsersService,
  ValidationsService
} from '../services'

export const init = () => {
  const calendarsService = CalendarsService.getInstance()
  const csrfService = CSRFService.getInstance()
  const errorsService = ErrorsService.getInstance()
  const eventsService = EventsService.getInstance()
  const followersService = FollowersService.getInstance()
  const sessionsService = SessionsService.getInstance()
  const usersService = UsersService.getInstance()
  const rsvpsService = RSVPsService.getInstance()
  const notificationsService = NotificationsService.getInstance()
  const validationsService = ValidationsService.getInstance()

  CalendarsController.getInstance(validationsService, calendarsService)
  ErrorsController.getInstance(errorsService)
  EventsController.getInstance(validationsService, eventsService, calendarsService)
  FollowersController.getInstance(validationsService, followersService, calendarsService)
  RSVPsController.getInstance(validationsService, rsvpsService, eventsService)
  SessionsController.getInstance(validationsService, csrfService, sessionsService, usersService)
  UsersController.getInstance(validationsService, usersService)
  NotificationsController.getInstance(validationsService, notificationsService)
}
