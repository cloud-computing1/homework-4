import { Database } from '../database'

export const init = async () => {
  const database = Database.getInstance()

  await database.getClient()
}
