import { Cache } from '../cache'
import { CSRFRepository, SessionsRepository } from '../cache/repositories'
import {
  CalendarsRepository,
  EventsRepository,
  FollowersRepository,
  RSVPsRepository,
  UsersRepository
} from '../database/repositories'

export const init = () => {
  const cache = Cache.getInstance()

  SessionsRepository.getInstance(cache)
  CSRFRepository.getInstance(cache)

  CalendarsRepository.getInstance()
  EventsRepository.getInstance()
  FollowersRepository.getInstance()
  RSVPsRepository.getInstance()
  UsersRepository.getInstance()
}
