import http from 'http'
import https from 'https'
import express from 'express'
import { initCache, initControllers, initDatabase, initProc, initProxy, initRepositories, initRoutes, initServer, initServices } from './init'

export async function app (): Promise<http.Server | https.Server> {
  const server = express()

  initProc()
  await initDatabase()
  initCache()
  initProxy()
  initRepositories()
  initServices()
  initControllers()
  initRoutes()

  return initServer(server)
}
