import firebase from 'firebase-admin'
import { env } from '../config'

export class FirebaseProxy {
  private static instance: FirebaseProxy | null;
  private creds: firebase.credential.Credential;
  private app: firebase.app.App;
  private messaging: firebase.messaging.Messaging;

  private constructor () {
    this.creds = firebase.credential.cert({
      projectId: env.values.FIREBASE_PROJECT_ID,
      privateKey: env.values.FIREBASE_PRIVATE_KEY,
      clientEmail: env.values.FIREBASE_CLIENT_EMAIL,
    })
    this.app = firebase.initializeApp({ credential: this.creds })
    this.messaging = this.app.messaging()
  }

  static getInstance () {
    if (!FirebaseProxy.instance) {
      FirebaseProxy.instance = new FirebaseProxy()
    }

    return FirebaseProxy.instance
  }

  async subscribe (topic: string, token: string) {
    await this.messaging.subscribeToTopic(token, topic)
  }

  async send (userId: string, payload: firebase.messaging.MessagingPayload) {
    await this.messaging.sendToTopic(`users.${userId}`, payload)
  }
}
