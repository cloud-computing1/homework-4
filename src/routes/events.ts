import { EventsController, SessionsController } from '../controllers'
import express from 'express'
import { Method, Verb } from '../typings'
import { asyncRouter } from '../utils'

export class EventsRoutes {
  private static instance: EventsRoutes | null;
  private sessionsController: SessionsController;
  private eventsController: EventsController;
  private router: express.Router;
  private upgradedRouter: Record<Verb, Method>;

  private constructor (sessionsController: SessionsController, eventsController: EventsController) {
    this.sessionsController = sessionsController
    this.eventsController = eventsController
    this.router = express.Router()
    this.upgradedRouter = asyncRouter(this.router)

    this.initRouter()
  }

  static getInstance (sessionsController?: SessionsController, eventsController?: EventsController) {
    if (!EventsRoutes.instance) {
      if (!sessionsController || !eventsController) {
        throw new Error('[EventsRoutes] Init error')
      }

      EventsRoutes.instance = new EventsRoutes(sessionsController, eventsController)
    }

    return EventsRoutes.instance
  }

  private initRouter () {
    this.upgradedRouter.get(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.eventsController.list.bind(this.eventsController)
    )
    this.upgradedRouter.post(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.eventsController.create.bind(this.eventsController)
    )
    this.upgradedRouter.patch(
      '/:eventId',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.eventsController.editEvent.bind(this.eventsController)
    )
    this.upgradedRouter.delete(
      '/:eventId',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.eventsController.removeEvent.bind(this.eventsController)
    )
  }

  getRouter () {
    return this.router
  }
}
