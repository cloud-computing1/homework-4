import { CalendarsController, EventsController, FollowersController, SessionsController } from '../controllers'
import express from 'express'
import { Method, Verb } from '../typings'
import { asyncRouter } from '../utils'

export class CalendarsRoutes {
  private static instance: CalendarsRoutes | null;
  private sessionsController: SessionsController;
  private calendarsController: CalendarsController;
  private eventsController: EventsController;
  private followersController: FollowersController
  private router: express.Router;
  private upgradedRouter: Record<Verb, Method>;

  private constructor (
    sessionsController: SessionsController,
    calendarsController: CalendarsController,
    eventsController: EventsController,
    followersController: FollowersController
  ) {
    this.sessionsController = sessionsController
    this.calendarsController = calendarsController
    this.eventsController = eventsController
    this.followersController = followersController
    this.router = express.Router()
    this.upgradedRouter = asyncRouter(this.router)

    this.initRouter()
  }

  static getInstance (
    sessionsController?: SessionsController,
    calendarsController?: CalendarsController,
    eventsController?: EventsController,
    followersController?: FollowersController
  ) {
    if (!CalendarsRoutes.instance) {
      if (!sessionsController || !calendarsController || !eventsController || !followersController) {
        throw new Error('[CalendarsRoutes] Init error')
      }

      CalendarsRoutes.instance = new CalendarsRoutes(sessionsController, calendarsController, eventsController, followersController)
    }

    return CalendarsRoutes.instance
  }

  private initRouter () {
    this.upgradedRouter.post(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.calendarsController.create.bind(this.calendarsController)
    )
    this.upgradedRouter.get(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.calendarsController.list.bind(this.calendarsController)
    )
    this.upgradedRouter.patch(
      '/:calendarId',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.calendarsController.editCalendar.bind(this.calendarsController)
    )
    this.upgradedRouter.delete(
      '/:calendarId',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.calendarsController.removeCalendar.bind(this.calendarsController)
    )
    this.upgradedRouter.get(
      '/:calendarId/events',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.eventsController.getForCalendar.bind(this.eventsController)
    )
    this.upgradedRouter.get(
      '/:calendarId/followers',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.followersController.getByCalendarId.bind(this.followersController)
    )
  }

  getRouter () {
    return this.router
  }
}
