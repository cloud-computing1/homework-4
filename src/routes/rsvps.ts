import { RSVPsController, SessionsController } from '../controllers'
import express from 'express'
import { Method, Verb } from '../typings'
import { asyncRouter } from '../utils'

export class RSVPsRoutes {
  private static instance: RSVPsRoutes | null;
  private sessionsController: SessionsController;
  private rsvpsController: RSVPsController;
  private router: express.Router;
  private upgradedRouter: Record<Verb, Method>;

  private constructor (sessionsController: SessionsController, rsvpsController: RSVPsController) {
    this.sessionsController = sessionsController
    this.rsvpsController = rsvpsController
    this.router = express.Router()
    this.upgradedRouter = asyncRouter(this.router)

    this.initRouter()
  }

  static getInstance (sessionsController?: SessionsController, rsvpsController?: RSVPsController) {
    if (!RSVPsRoutes.instance) {
      if (!sessionsController || !rsvpsController) {
        throw new Error('[RSVPsRoutes] Init error')
      }

      RSVPsRoutes.instance = new RSVPsRoutes(sessionsController, rsvpsController)
    }

    return RSVPsRoutes.instance
  }

  private initRouter () {
    this.upgradedRouter.post(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.rsvpsController.create.bind(this.rsvpsController)
    )
    this.upgradedRouter.patch(
      '/:rsvpId',
      this.sessionsController.getUser.bind(this.sessionsController),
      this.rsvpsController.edit.bind(this.rsvpsController)
    )
    this.upgradedRouter.delete(
      '/:rsvpId',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.rsvpsController.remove.bind(this.rsvpsController)
    )
  }

  getRouter () {
    return this.router
  }
}
