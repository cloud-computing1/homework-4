import express from 'express'
import { NotificationsController, SessionsController } from '../controllers'
import { Method, Verb } from '../typings'
import { asyncRouter } from '../utils'

export class NotificationsRoutes {
  private static instance: NotificationsRoutes | null;
  private sessionsController: SessionsController;
  private notificationsController: NotificationsController;
  private router: express.Router;
  private upgradedRouter: Record<Verb, Method>;

  private constructor (sessionsController: SessionsController, notificationsController: NotificationsController) {
    this.sessionsController = sessionsController
    this.notificationsController = notificationsController
    this.router = express.Router()
    this.upgradedRouter = asyncRouter(this.router)

    this.initRouter()
  }

  static getInstance (sessionsController?: SessionsController, notificationsController?: NotificationsController) {
    if (!NotificationsRoutes.instance) {
      if (!sessionsController || !notificationsController) {
        throw new Error('[NotificationsRoutes] Init error')
      }

      NotificationsRoutes.instance = new NotificationsRoutes(sessionsController, notificationsController)
    }

    return NotificationsRoutes.instance
  }

  private initRouter () {
    this.upgradedRouter.post(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.notificationsController.subscribe.bind(this.notificationsController)
    )
  }

  getRouter () {
    return this.router
  }
}
