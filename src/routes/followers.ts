import { FollowersController, SessionsController } from '../controllers'
import express from 'express'
import { Method, Verb } from '../typings'
import { asyncRouter } from '../utils'

export class FollowersRoutes {
  private static instance: FollowersRoutes | null;
  private sessionsController: SessionsController;
  private followersController: FollowersController;
  private router: express.Router;
  private upgradedRouter: Record<Verb, Method>;

  private constructor (sessionsController: SessionsController, followersController: FollowersController) {
    this.sessionsController = sessionsController
    this.followersController = followersController
    this.router = express.Router()
    this.upgradedRouter = asyncRouter(this.router)

    this.initRouter()
  }

  static getInstance (sessionsController?: SessionsController, followersController?: FollowersController) {
    if (!FollowersRoutes.instance) {
      if (!sessionsController || !followersController) {
        throw new Error('[FollowersRoutes] Init error')
      }

      FollowersRoutes.instance = new FollowersRoutes(sessionsController, followersController)
    }

    return FollowersRoutes.instance
  }

  private initRouter () {
    this.upgradedRouter.post(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.followersController.create.bind(this.followersController)
    )
    this.upgradedRouter.delete(
      '/:followerId',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.followersController.remove.bind(this.followersController)
    )
  }

  getRouter () {
    return this.router
  }
}
