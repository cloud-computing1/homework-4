import { SessionsController } from '../controllers/sessions'
import express from 'express'
import { Method, Verb } from '../typings'
import { asyncRouter } from '../utils'

export class SessionsRoutes {
  private static instance: SessionsRoutes | null;
  private sessionsController: SessionsController;
  private router: express.Router;
  private upgradedRouter: Record<Verb, Method>;

  private constructor (sessionsController: SessionsController) {
    this.sessionsController = sessionsController
    this.router = express.Router()
    this.upgradedRouter = asyncRouter(this.router)

    this.initRouter()
  }

  static getInstance (sessionsController?: SessionsController) {
    if (!SessionsRoutes.instance) {
      if (!sessionsController) {
        throw new Error('[SessionsRoutes] Init error')
      }

      SessionsRoutes.instance = new SessionsRoutes(sessionsController)
    }

    return SessionsRoutes.instance
  }

  private initRouter () {
    this.upgradedRouter.post(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.create.bind(this.sessionsController)
    )
  }

  getRouter () {
    return this.router
  }
}
