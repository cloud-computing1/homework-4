import express from 'express'
import { UsersController } from '../controllers'
import { SessionsController } from '../controllers/sessions'
import { Method, Verb } from '../typings'
import { asyncRouter } from '../utils'

export class UsersRoutes {
  private static instance: UsersRoutes | null;
  private sessionsController: SessionsController;
  private usersController: UsersController;
  private router: express.Router;
  private upgradedRouter: Record<Verb, Method>;

  private constructor (sessionsController: SessionsController, usersController: UsersController) {
    this.sessionsController = sessionsController
    this.usersController = usersController
    this.router = express.Router()
    this.upgradedRouter = asyncRouter(this.router)

    this.initRouter()
  }

  static getInstance (sessionsController?: SessionsController, usersController?: UsersController) {
    if (!UsersRoutes.instance) {
      if (!sessionsController || !usersController) {
        throw new Error('[UsersRoutes] Init error')
      }

      UsersRoutes.instance = new UsersRoutes(sessionsController, usersController)
    }

    return UsersRoutes.instance
  }

  private initRouter () {
    this.upgradedRouter.get(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.usersController.list.bind(this.usersController)
    )
    this.upgradedRouter.post(
      '/',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.usersController.create.bind(this.usersController)
    )
    this.upgradedRouter.get(
      '/:userId',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.usersController.getByUserId.bind(this.usersController)
    )
    this.upgradedRouter.patch(
      '/:userId',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.usersController.editUser.bind(this.usersController)
    )
    this.upgradedRouter.delete(
      '/:userId',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.sessionsController.getUser.bind(this.sessionsController),
      this.usersController.removeUser.bind(this.usersController)
    )
    this.upgradedRouter.post(
      '/password/resets',
      // this.sessionsController.checkCSRF.bind(this.sessionsController),
      this.usersController.resetPassword.bind(this.usersController)
    )
  }

  getRouter () {
    return this.router
  }
}
