import crypto from 'crypto'
import util from 'util'

export class CryptoService {
  private static instance: CryptoService | null;
  private randomBytes: (length: number) => Promise<Buffer>;

  private constructor () {
    this.randomBytes = util.promisify(crypto.randomBytes)
  }

  static getInstance () {
    if (!CryptoService.instance) {
      CryptoService.instance = new CryptoService()
    }

    return CryptoService.instance
  }

  async getRandomString (length: number) {
    const bytes = await this.randomBytes(length)
    return bytes.toString('hex')
  }

  async hash (text: string) {
    return crypto.createHash('sha512').update(text).digest('hex')
  }
}
