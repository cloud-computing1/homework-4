import { Calendar } from '../database/entities'
import { CalendarsRepository } from '../database/repositories'
import { CalendarVisibility, PublicCalendar, PublicUser } from '../typings'
import { enlist, notFoundError } from '../utils'
import { FollowersService } from './followers'
import { NotificationsService } from './notifications'
import { RSVPsService } from './rsvps'

export class CalendarsService {
  private static instance: CalendarsService | null;
  private calendarsRepository: CalendarsRepository;
  private followersService: FollowersService;
  private rsvpsService: RSVPsService;
  private notificationsService: NotificationsService;

  private constructor (
    calendarsRepository: CalendarsRepository,
    followersService: FollowersService,
    rsvpsService: RSVPsService,
    notificationsService: NotificationsService
  ) {
    this.calendarsRepository = calendarsRepository
    this.followersService = followersService
    this.rsvpsService = rsvpsService
    this.notificationsService = notificationsService
  }

  static getInstance (
    calendarsRepository?: CalendarsRepository,
    followersService?: FollowersService,
    rsvpsService?: RSVPsService,
    notificationsService?: NotificationsService
  ) {
    if (!CalendarsService.instance) {
      if (!calendarsRepository || !followersService || !rsvpsService || !notificationsService) {
        throw new Error('[CalendarsService] Init error')
      }

      CalendarsService.instance = new CalendarsService(
        calendarsRepository,
        followersService,
        rsvpsService,
        notificationsService
      )
    }

    return CalendarsService.instance
  }

  private map (entity: Calendar): PublicCalendar {
    return {
      calendarId: entity.calendarId,
      ownerUserId: entity.ownerUserId,
      name: entity.name,
      description: entity.description,
      visibility: entity.visibility,
      updateDate: entity.updateDate,
      createDate: entity.createDate
    }
  }

  async create (ownerUser: PublicUser, name: string, description: string, visibility: CalendarVisibility) {
    const calendar = await this.calendarsRepository.create(ownerUser.userId, name, description, visibility)

    await this.followersService.create(ownerUser, this.map(calendar))

    return this.map(calendar)
  }

  async getByCalendarId (calendarId: Calendar['calendarId']) {
    const calendar = await this.calendarsRepository.getByCalendarId(calendarId)
    return calendar && this.map(calendar)
  }

  async getOwnedByUser (ownerUser: PublicUser, skip: number, take: number) {
    const calendars = await this.calendarsRepository.getOwnedByUserId(ownerUser.userId, skip, take)
    return calendars.map((calendar) => this.map(calendar))
  }

  async getPublic (skip: number, take: number) {
    const calendars = await this.calendarsRepository.getPublic(skip, take)
    return calendars.map((calendar) => this.map(calendar))
  }

  async getPrivate (user: PublicUser, skip: number, take: number) {
    const calendars = await this.calendarsRepository.getPrivate(user.userId, skip, take)
    return calendars.map((calendar) => this.map(calendar))
  }

  async edit (calendarId: Calendar['calendarId'], name: string, description: string, visibility: CalendarVisibility) {
    const currentCalendar = await this.calendarsRepository.getByCalendarId(calendarId)

    if (!currentCalendar) {
      throw notFoundError()
    }

    await this.calendarsRepository.edit(
      calendarId,
      name || currentCalendar.name,
      description || currentCalendar.description,
      visibility || currentCalendar.visibility
    )
  }

  async remove (calendarId: Calendar['calendarId']) {
    await this.calendarsRepository.remove(calendarId)
  }

  async notifyCalendarUpdated (oldCalendar: PublicCalendar, newCalendar: PublicCalendar, actioner: PublicUser) {
    const followers = await this.followersService.getByCalendarId(newCalendar)
    const userIds = followers.map((follower) => follower.userId).filter((userId) => userId !== actioner.userId)

    const changes = enlist([
      ...(oldCalendar.name !== newCalendar.name ? [`title changed to ${newCalendar.name}`] : []),
      ...(oldCalendar.description !== newCalendar.description ? ['description was updated'] : []),
      ...(oldCalendar.visibility !== newCalendar.visibility ? [`visibility was set to ${newCalendar.visibility}`] : [])
    ])

    const title = `Calendar ${oldCalendar.name} was updated.`
    const body = changes ? `Its ${changes}.` : 'Nothing was changed though...'

    await this.notificationsService.sendToUsers(userIds, title, body)
  }

  async notifyCalendarDeleted (calendar: PublicCalendar, actioner: PublicUser) {
    const followers = await this.followersService.getByCalendarId(calendar)
    const rsvps = await this.rsvpsService.getByCalendarId(calendar)
    const userIds = [...followers.map((follower) => follower.userId), ...rsvps.map((rsvp) => rsvp.userId)]
      .filter((userIdA, index, array) => !array.includes(userIdA, index + 1))
      .filter((userId) => userId !== actioner.userId)

    const title = `Calendar ${calendar.name} was deleted.`
    const body = ''

    await this.notificationsService.sendToUsers(userIds, title, body)
  }
}
