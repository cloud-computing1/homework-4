import { CSRFRepository } from '../cache/repositories'
import { CryptoService } from './crypto'

export class CSRFService {
  private static instance: CSRFService | null;
  private csrfRepository: CSRFRepository;
  private cryptoService: CryptoService;

  private constructor (csrfRepository: CSRFRepository, cryptoService: CryptoService) {
    this.csrfRepository = csrfRepository
    this.cryptoService = cryptoService
  }

  static getInstance (csrfRepository?: CSRFRepository, cryptoService?: CryptoService) {
    if (!CSRFService.instance) {
      if (!csrfRepository || !cryptoService) {
        throw new Error('[CSRFService] Init error')
      }

      CSRFService.instance = new CSRFService(csrfRepository, cryptoService)
    }

    return CSRFService.instance
  }

  private map (entity: string) {
    return entity
  }

  async create () {
    const value = await this.cryptoService.getRandomString(24)
    await this.csrfRepository.create(value)

    return this.map(value)
  }

  async check (value: string) {
    const entry = await this.csrfRepository.check(value)
    return entry !== null
  }

  async clear () {
    await this.csrfRepository.clear()
  }
}
