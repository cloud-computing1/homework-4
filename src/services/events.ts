import { Event } from '../database/entities'
import { EventsRepository } from '../database/repositories'
import { PublicCalendar, PublicEvent, PublicUser, RSVPStatus } from '../typings'
import { enlist, notFoundError } from '../utils'
import { NotificationsService } from './notifications'
import { RSVPsService } from './rsvps'

export class EventsService {
  private static instance: EventsService | null;
  private eventsRepository: EventsRepository;
  private rsvpsService: RSVPsService;
  private notificationsService: NotificationsService;

  private constructor (
    eventsRepository: EventsRepository,
    rsvpsService: RSVPsService,
    notificationsService: NotificationsService
  ) {
    this.eventsRepository = eventsRepository
    this.rsvpsService = rsvpsService
    this.notificationsService = notificationsService
  }

  static getInstance (
    eventsRepository?: EventsRepository,
    rsvpsService?: RSVPsService,
    notificationsService?: NotificationsService
  ) {
    if (!EventsService.instance) {
      if (!eventsRepository || !rsvpsService || !notificationsService) {
        throw new Error('[EventsService] Init error')
      }

      EventsService.instance = new EventsService(eventsRepository, rsvpsService, notificationsService)
    }

    return EventsService.instance
  }

  private map (entity: Event): PublicEvent {
    return {
      eventId: entity.eventId,
      ownerUserId: entity.ownerUserId,
      ownerCalendarId: entity.ownerCalendarId,
      name: entity.name,
      description: entity.description,
      startDate: entity.startDate,
      endDate: entity.endDate,
      updateDate: entity.updateDate,
      createDate: entity.createDate
    }
  }

  async create (
    ownerUser: PublicUser,
    ownerCalendar: PublicCalendar,
    name: string,
    description: string,
    startDate: Date,
    endDate: Date
  ) {
    const event = await this.eventsRepository.create(
      ownerUser.userId,
      ownerCalendar.calendarId,
      name,
      description,
      startDate,
      endDate
    )

    await this.rsvpsService.create(ownerUser, this.map(event), RSVPStatus.Going)

    return this.map(event)
  }

  async getByEventId (eventId: Event['eventId']) {
    const event = await this.eventsRepository.getByEventId(eventId)
    return event && this.map(event)
  }

  async getByUserId (user: PublicUser, skip: number, take: number) {
    const events = await this.eventsRepository.getByUserId(user.userId, skip, take)
    return events.map((event) => this.map(event))
  }

  async getByUserIdAndCalendarId (user: PublicUser, calendar: PublicCalendar, skip: number, take: number) {
    const events = await this.eventsRepository.getByUserIdAndCalendarId(user.userId, calendar.calendarId, skip, take)
    return events.map((event) => this.map(event))
  }

  async edit (eventId: Event['eventId'], name: string, description: string, startDate: string, endDate: string) {
    const currentEvent = await this.eventsRepository.getByEventId(eventId)

    if (!currentEvent) {
      throw notFoundError()
    }

    return this.eventsRepository.edit(
      eventId,
      name || currentEvent.name,
      description || currentEvent.description,
      new Date(startDate) || currentEvent.startDate,
      new Date(endDate) || currentEvent.endDate
    )
  }

  async remove (eventId: Event['eventId']) {
    return this.eventsRepository.remove(eventId)
  }

  async notifyEventUpdated (oldEvent: PublicEvent, newEvent: PublicEvent, actioner: PublicUser) {
    const rsvps = await this.rsvpsService.getByEventId(oldEvent)
    const userIds = rsvps.map((rsvp) => rsvp.userId).filter((userId) => userId !== actioner.userId)

    const changes = enlist([
      ...(oldEvent.name !== newEvent.name ? [`title changed to ${newEvent.name}`] : []),
      ...(oldEvent.description !== newEvent.description ? ['description was updated'] : []),
      ...(oldEvent.startDate !== newEvent.startDate ? [`start date was set to ${newEvent.startDate}`] : []),
      ...(oldEvent.startDate !== newEvent.startDate ? [`end date was set to ${newEvent.endDate}`] : [])
    ])

    const title = `Event ${oldEvent.name} was updated.`
    const body = `Its ${changes}.`

    await this.notificationsService.sendToUsers(userIds, title, body)
  }

  async notifyEventDeleted (event: PublicEvent, actioner: PublicUser) {
    const rsvps = await this.rsvpsService.getByEventId(event)
    const userIds = rsvps.map((rsvp) => rsvp.userId).filter((userId) => userId !== actioner.userId)

    const title = `Calendar ${event.name} was deleted.`
    const body = ''

    await this.notificationsService.sendToUsers(userIds, title, body)
  }
}
