import { SessionsRepository } from '../cache/repositories'
import { PublicUser } from '../typings'
import { PublicSession } from '../typings/sessions'
import { CryptoService } from './crypto'

export class SessionsService {
  private static instance: SessionsService | null;
  private sessionsRepository: SessionsRepository;
  private cryptoService: CryptoService;

  private constructor (sessionsRepository: SessionsRepository, cryptoService: CryptoService) {
    this.sessionsRepository = sessionsRepository
    this.cryptoService = cryptoService
  }

  static getInstance (sessionsRepository?: SessionsRepository, cryptoService?: CryptoService) {
    if (!SessionsService.instance) {
      if (!sessionsRepository || !cryptoService) {
        throw new Error('[SessionsService] Init error')
      }

      SessionsService.instance = new SessionsService(sessionsRepository, cryptoService)
    }

    return SessionsService.instance
  }

  private map (user: PublicUser, entity: string): PublicSession {
    return { userId: user.userId, session: entity }
  }

  async create (user: PublicUser) {
    const token = await this.cryptoService.getRandomString(32)
    await this.sessionsRepository.create(user.userId, token)

    return this.map(user, token)
  }

  async getUser (token: string) {
    return this.sessionsRepository.get(token)
  }

  async check (token: string) {
    const session = await this.sessionsRepository.get(token)
    return session !== null
  }

  async remove (token: string) {
    await this.sessionsRepository.remove(token)
  }
}
