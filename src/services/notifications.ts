import { PublicUser } from 'typings'
import { array } from '../utils'
import { FirebaseProxy } from '../proxy'

export class NotificationsService {
  private static instance: NotificationsService | null;
  private firebaseProxy: FirebaseProxy;

  private constructor (firebaseProxy: FirebaseProxy) {
    this.firebaseProxy = firebaseProxy
  }

  static getInstance (firebaseProxy?: FirebaseProxy) {
    if (!NotificationsService.instance) {
      if (!firebaseProxy) {
        throw new Error('[NotificationsService] Init error')
      }

      NotificationsService.instance = new NotificationsService(firebaseProxy)
    }

    return NotificationsService.instance
  }

  async subscribe (user: PublicUser, token: string) {
    await this.firebaseProxy.subscribe(`users.${user.userId}`, token)
  }

  async sendToUser (userId: string, title: string, message: string) {
    await this.firebaseProxy.send(userId, {
      notification: {
        title,
        body: message
      }
    })
  }

  async sendToUsers (userIds: string[], title: string, message: string) {
    await array(userIds).mapPromises((userId) => this.sendToUser(userId, title, message))
  }
}
