import { RSVP } from '../database/entities'
import { RSVPsRepository } from '../database/repositories'
import { PublicCalendar, PublicEvent, PublicRSVP, PublicUser, RSVPStatus } from '../typings'
import { internalServerError } from '../utils'
import { NotificationsService } from './notifications'

export class RSVPsService {
  private static instance: RSVPsService | null;
  private rsvpsRepository: RSVPsRepository;
  private notificationsService: NotificationsService;

  private constructor (rsvpsRepository: RSVPsRepository, notificationsService: NotificationsService) {
    this.rsvpsRepository = rsvpsRepository
    this.notificationsService = notificationsService
  }

  static getInstance (rsvpsRepository?: RSVPsRepository, notificationsService?: NotificationsService) {
    if (!RSVPsService.instance) {
      if (!rsvpsRepository || !notificationsService) {
        throw new Error('[RSVPsService] Init error')
      }

      RSVPsService.instance = new RSVPsService(rsvpsRepository, notificationsService)
    }

    return RSVPsService.instance
  }

  private map (entity: RSVP): PublicRSVP {
    return {
      rsvpId: entity.rsvpId,
      userId: entity.userId,
      eventId: entity.eventId,
      status: entity.status,
      createDate: entity.createDate,
      updateDate: entity.updateDate
    }
  }

  async create (user: PublicUser, event: PublicEvent, status: RSVPStatus) {
    const rsvp = await this.rsvpsRepository.create(user.userId, event.eventId, status)
    return this.map(rsvp)
  }

  async getByRsvpId (rsvpId: RSVP['rsvpId']) {
    const rsvp = await this.rsvpsRepository.getByRsvpId(rsvpId)
    return rsvp && this.map(rsvp)
  }

  async getByUserId (user: PublicUser) {
    const rsvps = await this.rsvpsRepository.getByUserId(user.userId)
    return rsvps.map((rsvp) => this.map(rsvp))
  }

  async getByEventId (event: PublicEvent) {
    const rsvps = await this.rsvpsRepository.getByEventId(event.eventId)
    return rsvps.map((rsvp) => this.map(rsvp))
  }

  async getByCalendarId (calendar: PublicCalendar) {
    const rsvps = await this.rsvpsRepository.getByCalendarId(calendar.calendarId)
    return rsvps.map((rsvp) => this.map(rsvp))
  }

  async getByUserIdAndEventId (user: PublicUser, event: PublicEvent) {
    const rsvp = await this.rsvpsRepository.getByUserIdAndEventId(user.userId, event.eventId)
    return rsvp && this.map(rsvp)
  }

  async edit (rsvpId: RSVP['rsvpId'], status: RSVPStatus) {
    await this.rsvpsRepository.setStatus(rsvpId, status)

    const rsvp = await this.rsvpsRepository.getByRsvpId(rsvpId)

    if (!rsvp) {
      throw internalServerError()
    }

    return this.map(rsvp)
  }

  async remove (rsvpId: RSVP['rsvpId']) {
    await this.rsvpsRepository.remove(rsvpId)
  }

  async notifyRSVPCreated (event: PublicEvent, actioner: PublicUser) {
    const rsvps = await this.getByEventId(event)
    const userIds = rsvps.map((rsvp) => rsvp.userId).filter((userId) => userId !== actioner.userId)

    const title = 'New event follower'
    const body = `${actioner.firstName} is following your event, "${event.name}".`

    await this.notificationsService.sendToUsers(userIds, title, body)
  }

  async notifyRSVPUpdated (event: PublicEvent, rsvp: PublicRSVP, actioner: PublicUser) {
    const rsvps = await this.getByEventId(event)
    const userIds = rsvps.map((rsvp) => rsvp.userId).filter((userId) => userId !== actioner.userId)

    const title = 'Event follower updated its status'
    const body = `${actioner.firstName} set his status to "${rsvp.status}" for event your event, "${event.name}".`

    await this.notificationsService.sendToUsers(userIds, title, body)
  }

  async notifyRSVPRemoved (event: PublicEvent, actioner: PublicUser) {
    const rsvps = await this.getByEventId(event)
    const userIds = rsvps.map((rsvp) => rsvp.userId).filter((userId) => userId !== actioner.userId)

    const title = 'User unfollowed an event'
    const body = `${actioner.firstName} is not following your event "${event.name}" anymore.`

    await this.notificationsService.sendToUsers(userIds, title, body)
  }
}
