import { HTTPError } from '../utils'

export class ErrorsService {
  private static instance: ErrorsService | null;

  static getInstance () {
    if (!ErrorsService.instance) {
      ErrorsService.instance = new ErrorsService()
    }

    return ErrorsService.instance
  }

  handle (err: HTTPError) {
    return err.print()
  }
}
