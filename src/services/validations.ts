import { regex } from '../config'

export class ValidationsService {
  private static instance: ValidationsService | null;

  static getInstance () {
    if (!ValidationsService.instance) {
      ValidationsService.instance = new ValidationsService()
    }

    return ValidationsService.instance
  }

  email (text: string) {
    return regex.EMAIL.test(text)
  }

  password (text: string) {
    return regex.PASSWORD.bigLetter.test(text) && regex.PASSWORD.numbers.test(text) && regex.PASSWORD.length.test(text)
  }

  isNotEmptyText (text: undefined | string) {
    return text !== undefined && text.length > 0
  }

  isValidNumber (number: number) {
    return !Number.isNaN(number)
  }

  isNotDate (text: string) {
    const timestamp = Date.parse(text)
    return Number.isNaN(timestamp)
  }

  isUUID (text: string) {
    return regex.UUID.test(text)
  }
}
