import { Follower } from '../database/entities'
import { FollowersRepository } from '../database/repositories'
import { PublicCalendar, PublicFollower, PublicUser } from '../typings'
import { NotificationsService } from './notifications'

export class FollowersService {
  private static instance: FollowersService | null;
  private followersRepository: FollowersRepository;
  private notificationsService: NotificationsService;

  private constructor (followersRepository: FollowersRepository, notificationsService: NotificationsService) {
    this.followersRepository = followersRepository
    this.notificationsService = notificationsService
  }

  static getInstance (followersRepository?: FollowersRepository, notificationsService?: NotificationsService) {
    if (!FollowersService.instance) {
      if (!followersRepository || !notificationsService) {
        throw new Error('[FollowersService] Init error')
      }

      FollowersService.instance = new FollowersService(followersRepository, notificationsService)
    }

    return FollowersService.instance
  }

  private map (entity: Follower): PublicFollower {
    return {
      followerId: entity.followerId,
      userId: entity.userId,
      calendarId: entity.calendarId,
      createDate: entity.createDate,
      updateDate: entity.updateDate
    }
  }

  async create (user: PublicUser, calendar: PublicCalendar) {
    const follower = await this.followersRepository.add(user.userId, calendar.calendarId)
    return this.map(follower)
  }

  async getByFollowerId (followerId: Follower['followerId']) {
    const follower = await this.followersRepository.getByFollowerId(followerId)
    return follower && this.map(follower)
  }

  async getByUserAndCalendar (user: PublicUser, calendar: PublicCalendar) {
    const follower = await this.followersRepository.getByUserIdAndCalendarId(user.userId, calendar.calendarId)
    return follower && this.map(follower)
  }

  async getByUserId (user: PublicUser) {
    const followers = await this.followersRepository.getByUserId(user.userId)
    return followers.map((follower) => this.map(follower))
  }

  async getByCalendarId (calendar: PublicCalendar) {
    const followers = await this.followersRepository.getByCalendarId(calendar.calendarId)
    return followers.map((follower) => this.map(follower))
  }

  async remove (followerId: Follower['followerId']) {
    await this.followersRepository.remove(followerId)
  }

  async notifyFollowerCreated (calendar: PublicCalendar, actioner: PublicUser) {
    const rsvps = await this.getByCalendarId(calendar)
    const userIds = rsvps.map((rsvp) => rsvp.userId).filter((userId) => userId !== actioner.userId)

    const title = 'New calendar follower'
    const body = `${actioner.firstName} is following your calendar, "${calendar.name}".`

    await this.notificationsService.sendToUsers(userIds, title, body)
  }

  async notifyFollowerRemoved (calendar: PublicCalendar, actioner: PublicUser) {
    const rsvps = await this.getByCalendarId(calendar)
    const userIds = rsvps.map((rsvp) => rsvp.userId).filter((userId) => userId !== actioner.userId)

    const title = 'User unfollowed a calendar'
    const body = `${actioner.firstName} is not following your calendar "${calendar.name}" anymore.`

    await this.notificationsService.sendToUsers(userIds, title, body)
  }
}
