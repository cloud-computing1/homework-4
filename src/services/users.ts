import { notFoundError, notImplementedError } from '../utils'
import { constants } from '../config'
import { User } from '../database/entities'
import { UsersRepository } from '../database/repositories'
import { CalendarVisibility, PublicUser } from '../typings'
import { CalendarsService, CryptoService } from './'

export class UsersService {
  private static instance: UsersService | null;
  private usersRepository: UsersRepository;
  private cryptoService: CryptoService;
  private calendarsService: CalendarsService;

  private constructor (
    usersRepository: UsersRepository,
    cryptoService: CryptoService,
    calendarsService: CalendarsService
  ) {
    this.usersRepository = usersRepository
    this.cryptoService = cryptoService
    this.calendarsService = calendarsService
  }

  static getInstance (
    usersRepository?: UsersRepository,
    cryptoService?: CryptoService,
    calendarsService?: CalendarsService
  ) {
    if (!UsersService.instance) {
      if (!usersRepository || !cryptoService || !calendarsService) {
        throw new Error('[UsersService] Init error')
      }

      UsersService.instance = new UsersService(usersRepository, cryptoService, calendarsService)
    }

    return UsersService.instance
  }

  private map (entity: User): PublicUser {
    return {
      userId: entity.userId,
      email: entity.email,
      firstName: entity.firstName,
      lastName: entity.lastName,
      updateDate: entity.updateDate,
      createDate: entity.createDate
    }
  }

  async create (email: string, password: string, firstName: string, lastName: string) {
    const hashedPassword = await this.cryptoService.hash(password)
    const user = await this.usersRepository.create(email, hashedPassword, firstName, lastName)

    const publicUser = this.map(user)

    const calendarName = `${firstName}${constants.DEFAULT_CALENDAR_NAME}`
    const calendarDescription = constants.DEFAULT_CALENDAR_DESCRIPTION
    const calendarVisibility = CalendarVisibility.Private

    await this.calendarsService.create(publicUser, calendarName, calendarDescription, calendarVisibility)

    return publicUser
  }

  async getByUserId (userId: User['userId']) {
    const user = await this.usersRepository.getByUserId(userId)
    return user && this.map(user)
  }

  async getByEmailAndPassword (email: User['email'], password: User['password']) {
    const hashedPassword = await this.cryptoService.hash(password)
    const user = await this.usersRepository.getByEmailAndPassword(email, hashedPassword)
    return user && this.map(user)
  }

  async getByEmail (email: User['email']) {
    const user = await this.usersRepository.getByEmail(email)
    return user && this.map(user)
  }

  async edit (userId: User['userId'], email: string, password: string, firstName: string, lastName: string) {
    const hashedPassword = await this.cryptoService.hash(password)
    const currentUser = await this.usersRepository.getByUserId(userId)

    if (!currentUser) {
      throw notFoundError()
    }

    await this.usersRepository.edit(
      userId,
      email || currentUser.email,
      hashedPassword || currentUser.password,
      firstName || currentUser.firstName,
      lastName || currentUser.lastName
    )
  }

  async remove (userId: User['userId']) {
    await this.usersRepository.remove(userId)
  }

  async resetPassword (_: string) {
    throw notImplementedError()
  }
}
