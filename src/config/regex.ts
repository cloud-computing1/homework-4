export const DASHES = /-/g
export const EMAIL = /^[\w\d]([\w\d_-]*[\w\d])?((\.[\w\d]([\w\d-]*[\w\d])?)+)+@[\w\d]([\w\d-]*[\w\d])?((\.[\w\d]([\w\d-]*[\w\d])?)+)*?(\.[\w\d]{2,})$/
export const PASSWORD = {
  numbers: /\d{2}/,
  bigLetter: /[A-Z]/,
  length: /[\w\d]{8,}/
}
export const UUID = /^[a-fA-F0-9]{32}$/
