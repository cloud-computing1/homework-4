import { tidyEnv } from 'tidyenv'

export const values = tidyEnv.process(process.env, {
  PORT: tidyEnv.num({ default: 3000 }),
  SSL_ENABLED: tidyEnv.bool({ default: false }),

  DATABASE_HOST: tidyEnv.str(),
  DATABASE_PORT: tidyEnv.num({ default: 5432 }),
  DATABASE_USERNAME: tidyEnv.str(),
  DATABASE_PASSWORD: tidyEnv.str(),
  DATABASE_NAME: tidyEnv.str(),

  REDIS_HOST: tidyEnv.str(),
  REDIS_PORT: tidyEnv.num(),
  REDIS_PASSWORD: tidyEnv.str(),
  REDIS_USE_TLS: tidyEnv.bool({ default: true }),

  FIREBASE_PROJECT_ID: tidyEnv.str(),
  FIREBASE_PRIVATE_KEY_ID: tidyEnv.str(),
  FIREBASE_PRIVATE_KEY: tidyEnv.str(),
  FIREBASE_CLIENT_EMAIL: tidyEnv.str(),
  FIREBASE_CLIENT_ID: tidyEnv.str(),
  FIREBASE_AUTH_URI: tidyEnv.str(),
  FIREBASE_TOKEN_URI: tidyEnv.str(),
  FIREBASE_AUTH_PROVIDER_X509_CERT_URL: tidyEnv.str(),
  FIREBASE_CLIENT_X509_CERT_URL: tidyEnv.str()
})
