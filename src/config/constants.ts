import { RedisOptions } from 'ioredis'
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'
import { HTTPErrorResponse } from '../typings'
import { env } from '../config'
import { Calendar, Event, Follower, RSVP, User } from '../database/entities'

export const DATABASE: PostgresConnectionOptions = {
  name: 'postgres',
  type: 'postgres',
  host: env.values.DATABASE_HOST,
  port: env.values.DATABASE_PORT,
  username: env.values.DATABASE_USERNAME,
  password: env.values.DATABASE_PASSWORD,
  database: env.values.DATABASE_NAME,
  ssl: true,
  entities: [Calendar, Event, Follower, RSVP, User]
}
export const REDIS: RedisOptions = {
  host: env.values.REDIS_HOST,
  port: env.values.REDIS_PORT,
  ...(env.values.REDIS_PASSWORD && { password: env.values.REDIS_PASSWORD }),
  ...(env.values.REDIS_USE_TLS && { tls: { host: env.values.REDIS_HOST } })
}

export const DEFAULT_PAGE_SIZE = 20

export const SESSIONS_REDIS_PREFIX = 'sessions'

export const GENERIC_500_ERROR: HTTPErrorResponse = { code: 500, message: 'Internal server error' }

export const CSRF_KEY_NAME = 'csrf'

export const DEFAULT_CSRF_TIME = 1800000
export const DEFAULT_EVENT_TIME = 3600000
export const DEFAULT_CALENDAR_NAME = "'s personal calendar"
export const DEFAULT_CALENDAR_DESCRIPTION = 'Home sweet home!'
