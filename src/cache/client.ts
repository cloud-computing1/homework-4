import { constants } from '../config'
import Ioredis from 'ioredis'

export class Cache {
  private static instance: Cache | null;
  private client: Ioredis.Redis;

  private constructor () {
    this.client = new Ioredis(constants.REDIS)
  }

  static getInstance () {
    if (!Cache.instance) {
      Cache.instance = new Cache()
    }

    return Cache.instance
  }

  getClient () {
    return this.client
  }
}
