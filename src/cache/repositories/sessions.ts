import { Cache } from '..'
import { constants } from '../../config'
import { Redis } from 'ioredis'

export class SessionsRepository {
  private static instance: SessionsRepository | null;
  private client: Redis;

  private constructor (cache: Cache) {
    this.client = cache.getClient()
  }

  static getInstance (cache?: Cache) {
    if (!SessionsRepository.instance) {
      if (!cache) {
        throw new Error('[SessionsRepository] Init error')
      }

      SessionsRepository.instance = new SessionsRepository(cache)
    }

    return SessionsRepository.instance
  }

  async create (userId: string, token: string) {
    await this.client.set(`${constants.SESSIONS_REDIS_PREFIX}:${token}`, userId)
  }

  async get (token: string) {
    return this.client.get(`${constants.SESSIONS_REDIS_PREFIX}:${token}`)
  }

  async remove (token: string) {
    await this.client.del(`${constants.SESSIONS_REDIS_PREFIX}:${token}`)
  }
}
