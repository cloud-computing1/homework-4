import { Cache } from '..'
import { constants } from '../../config'
import { Redis } from 'ioredis'

export class CSRFRepository {
  private static instance: CSRFRepository | null;
  private client: Redis;

  private constructor (cache: Cache) {
    this.client = cache.getClient()
  }

  static getInstance (cache?: Cache) {
    if (!CSRFRepository.instance) {
      if (!cache) {
        throw new Error('[CSRFRepository] Init error')
      }

      CSRFRepository.instance = new CSRFRepository(cache)
    }

    return CSRFRepository.instance
  }

  async create (value: string) {
    await this.client.hset(constants.CSRF_KEY_NAME, value, 'valid')
  }

  async check (value: string) {
    return this.client.hget(constants.CSRF_KEY_NAME, value)
  }

  async clear () {
    await this.client.del(constants.CSRF_KEY_NAME)
  }
}
