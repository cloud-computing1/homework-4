import express from 'express'
import { EventsService, RSVPsService, ValidationsService } from '../services'
import { RSVPStatus } from '../typings'
import { conflictError, internalServerError, notFoundError, parametersError } from '../utils'

export class RSVPsController {
  private static instance: RSVPsController | null;
  private validationsService: ValidationsService;
  private rsvpsService: RSVPsService;
  private eventsService: EventsService;

  private constructor (
    validationsService: ValidationsService,
    rsvpsService: RSVPsService,
    eventsService: EventsService
  ) {
    this.validationsService = validationsService
    this.rsvpsService = rsvpsService
    this.eventsService = eventsService
  }

  static getInstance (
    validationsService?: ValidationsService,
    rsvpsService?: RSVPsService,
    eventsService?: EventsService
  ) {
    if (!RSVPsController.instance) {
      if (!validationsService || !rsvpsService || !eventsService) {
        throw new Error('[RSVPsController] Init error')
      }

      RSVPsController.instance = new RSVPsController(validationsService, rsvpsService, eventsService)
    }

    return RSVPsController.instance
  }

  async create (req: express.Request, res: express.Response) {
    const user = req.user
    const eventId = req.body.eventId
    const status = RSVPStatus.Going

    if (!user) {
      throw parametersError()
    }

    if (!this.validationsService.isNotEmptyText(eventId)) {
      throw parametersError()
    }

    const event = await this.eventsService.getByEventId(eventId)

    if (!event) {
      throw parametersError()
    }

    const existingRsvp = await this.rsvpsService.getByUserIdAndEventId(user, event)

    if (existingRsvp) {
      throw conflictError()
    }

    const rsvp = await this.rsvpsService.create(user, event, status)

    await this.rsvpsService.notifyRSVPCreated(event, user)

    res.status(201).send(rsvp)
  }

  async edit (req: express.Request, res: express.Response) {
    const user = req.user
    const rsvpId = String(req.params.eventId)
    const status = RSVPStatus[req.body.status as keyof typeof RSVPStatus]

    if (!user) {
      throw parametersError()
    }

    if (!this.validationsService.isNotEmptyText(rsvpId) || !status) {
      throw parametersError()
    }

    const existingRsvp = await this.rsvpsService.getByRsvpId(rsvpId)

    if (!existingRsvp) {
      throw notFoundError()
    }

    const event = await this.eventsService.getByEventId(existingRsvp.eventId)

    if (!event) {
      throw internalServerError()
    }

    const rsvp = await this.rsvpsService.edit(rsvpId, status)

    await this.rsvpsService.notifyRSVPUpdated(event, rsvp, user)

    res.status(200).send(rsvp)
  }

  async remove (req: express.Request, res: express.Response) {
    const user = req.user
    const rsvpId = String(req.params.eventId)

    if (!user) {
      throw parametersError()
    }

    if (!this.validationsService.isNotEmptyText(rsvpId)) {
      throw parametersError()
    }

    const existingRsvp = await this.rsvpsService.getByRsvpId(rsvpId)

    if (!existingRsvp) {
      throw notFoundError()
    }

    const event = await this.eventsService.getByEventId(existingRsvp.eventId)

    if (!event) {
      throw internalServerError()
    }

    await this.rsvpsService.notifyRSVPRemoved(event, user)
    await this.rsvpsService.remove(rsvpId)

    res.status(204).send()
  }
}
