import { constants } from '../config'
import express from 'express'
import { ErrorsService } from '../services'
import { HTTPError, isHTTPError } from '../utils'

export class ErrorsController {
  private static instance: ErrorsController | null;
  private errorsService: ErrorsService;

  private constructor (errorsService: ErrorsService) {
    this.errorsService = errorsService
  }

  static getInstance (errorsService?: ErrorsService) {
    if (!ErrorsController.instance) {
      if (!errorsService) {
        throw new Error('[ErrorsController] Init error')
      }

      ErrorsController.instance = new ErrorsController(errorsService)
    }

    return ErrorsController.instance
  }

  handle (err: Error | HTTPError, _1: express.Request, res: express.Response, _2: express.NextFunction) {
    const isKnownError = isHTTPError(err)
    const response = isKnownError ? this.errorsService.handle(err as HTTPError) : constants.GENERIC_500_ERROR

    if (!isKnownError) {
      console.error(err)
    }

    res.status(response.code).send({ message: response.message })
  }
}
