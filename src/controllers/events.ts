import express from 'express'
import { constants } from '../config'
import { CalendarsService, EventsService, ValidationsService } from '../services'
import { forbiddenError, internalServerError, notFoundError, parametersError } from '../utils'

export class EventsController {
  private static instance: EventsController | null;
  private validationsService: ValidationsService;
  private eventsService: EventsService;
  private calendarsService: CalendarsService;

  private constructor (
    validationsService: ValidationsService,
    eventsService: EventsService,
    calendarsService: CalendarsService
  ) {
    this.validationsService = validationsService
    this.eventsService = eventsService
    this.calendarsService = calendarsService
  }

  static getInstance (
    validationsService?: ValidationsService,
    eventsService?: EventsService,
    calendarsService?: CalendarsService
  ) {
    if (!EventsController.instance) {
      if (!validationsService || !eventsService || !calendarsService) {
        throw new Error('[EventsController] Init error')
      }

      EventsController.instance = new EventsController(validationsService, eventsService, calendarsService)
    }

    return EventsController.instance
  }

  async list (req: express.Request, res: express.Response) {
    const user = req.user
    const skip = Number(req.query.skip || 0)
    const take = Number(req.query.take || constants.DEFAULT_PAGE_SIZE)

    if (!user) {
      throw parametersError()
    }

    if (!this.validationsService.isValidNumber(skip) || !this.validationsService.isValidNumber(take)) {
      throw parametersError()
    }

    const events = await this.eventsService.getByUserId(user, skip, take)

    res.status(200).send(events)
  }

  async create (req: express.Request, res: express.Response) {
    const user = req.user
    const calendarId = req.body.calendarId
    const name = req.body.name
    const description = req.body.description
    const startDate = req.body.startDate
    const endDate = req.body.endDate

    if (!user) {
      throw parametersError()
    }

    if (
      !this.validationsService.isNotEmptyText(calendarId) ||
      !this.validationsService.isNotEmptyText(name) ||
      !this.validationsService.isNotEmptyText(description) ||
      !this.validationsService.isNotEmptyText(startDate) ||
      !this.validationsService.isNotEmptyText(endDate) ||
      this.validationsService.isNotDate(startDate) ||
      this.validationsService.isNotDate(endDate)
    ) {
      throw parametersError()
    }

    const calendar = await this.calendarsService.getByCalendarId(calendarId)

    if (!calendar) {
      throw parametersError()
    }

    if (calendar.ownerUserId !== user.userId) {
      throw forbiddenError()
    }

    const event = await this.eventsService.create(
      user,
      calendar,
      name,
      description,
      new Date(startDate),
      new Date(endDate)
    )

    res.status(201).send(event)
  }

  async editEvent (req: express.Request, res: express.Response) {
    const user = req.user
    const eventId = req.params.eventId || ''
    const name = req.body.name || ''
    const description = req.body.description || ''
    const startDate = req.body.startDate || ''
    const endDate = req.body.endDate || ''

    if (
      !this.validationsService.isUUID(eventId) ||
      (this.validationsService.isNotEmptyText(startDate) && this.validationsService.isNotDate(startDate)) ||
      (this.validationsService.isNotEmptyText(endDate) && this.validationsService.isNotDate(endDate))
    ) {
      throw parametersError()
    }

    const event = await this.eventsService.getByEventId(eventId)

    if (!event) {
      throw notFoundError()
    }

    if (event.ownerUserId !== user?.userId) {
      throw forbiddenError()
    }

    await this.eventsService.edit(eventId, name, description, startDate, endDate)

    const updatedEvent = await this.eventsService.getByEventId(eventId)

    if (!updatedEvent) {
      throw internalServerError()
    }

    await this.eventsService.notifyEventUpdated(event, updatedEvent, user)

    res.status(200).send(updatedEvent)
  }

  async removeEvent (req: express.Request, res: express.Response) {
    const user = req.user
    const eventId = req.params.eventId || ''

    if (!this.validationsService.isUUID(eventId)) {
      throw parametersError()
    }

    const event = await this.eventsService.getByEventId(eventId)

    if (!event) {
      throw notFoundError()
    }

    if (event.ownerUserId !== user?.userId) {
      throw forbiddenError()
    }

    await this.eventsService.notifyEventDeleted(event, user)
    await this.eventsService.remove(eventId)

    res.status(204).send()
  }

  async getForCalendar (req: express.Request, res: express.Response) {
    const user = req.user
    const calendarId = String(req.params.calendarId)
    const skip = Number(req.query.skip || 0)
    const take = Number(req.query.take || constants.DEFAULT_PAGE_SIZE)

    if (!user) {
      throw parametersError()
    }

    if (
      !this.validationsService.isNotEmptyText(calendarId) ||
      !this.validationsService.isValidNumber(skip) ||
      !this.validationsService.isValidNumber(take)
    ) {
      throw parametersError()
    }

    const calendar = await this.calendarsService.getByCalendarId(calendarId)

    if (!calendar) {
      throw notFoundError()
    }

    const events = await this.eventsService.getByUserIdAndCalendarId(user, calendar, skip, take)

    res.status(200).send(events)
  }
}
