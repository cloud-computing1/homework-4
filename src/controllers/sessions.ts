import express from 'express'
import { CSRFService, SessionsService, UsersService, ValidationsService } from '../services'
import { parametersError, unauthorizedError } from '../utils'

export class SessionsController {
  private static instance: SessionsController | null;
  private validationsService: ValidationsService;
  private csrfService: CSRFService;
  private sessionsService: SessionsService;
  private usersService: UsersService;

  private constructor (
    validationsService: ValidationsService,
    csrfService: CSRFService,
    sessionsService: SessionsService,
    usersService: UsersService
  ) {
    this.validationsService = validationsService
    this.csrfService = csrfService
    this.sessionsService = sessionsService
    this.usersService = usersService
  }

  static getInstance (
    validationsService?: ValidationsService,
    csrfService?: CSRFService,
    sessionsService?: SessionsService,
    usersService?: UsersService
  ) {
    if (!SessionsController.instance) {
      if (!validationsService || !csrfService || !sessionsService || !usersService) {
        throw new Error('[SessionsController] Init error')
      }

      SessionsController.instance = new SessionsController(
        validationsService,
        csrfService,
        sessionsService,
        usersService
      )
    }

    return SessionsController.instance
  }

  async checkCSRF (req: express.Request, _: express.Response, next: express.NextFunction) {
    const csrf = req.headers.csrf ? (Array.isArray(req.headers.csrf) ? req.headers.csrf[0] : req.headers.csrf) : ''

    if (!csrf) {
      throw unauthorizedError()
    }

    const valid = await this.csrfService.check(csrf)

    if (!valid) {
      throw unauthorizedError()
    }

    next()
  }

  async checkSession (req: express.Request, _: express.Response, next: express.NextFunction) {
    const session = req.headers.session
      ? Array.isArray(req.headers.session)
        ? req.headers.session[0]
        : req.headers.session
      : ''

    if (!session) {
      throw unauthorizedError()
    }

    const valid = await this.sessionsService.check(session)

    if (!valid) {
      throw unauthorizedError()
    }

    next()
  }

  async getUser (req: express.Request, _: express.Response, next: express.NextFunction) {
    const session = req.headers.session
      ? Array.isArray(req.headers.session)
        ? req.headers.session[0]
        : req.headers.session
      : ''

    if (!session) {
      throw unauthorizedError()
    }

    const userId = await this.sessionsService.getUser(session)

    if (!userId) {
      throw unauthorizedError()
    }

    const user = await this.usersService.getByUserId(userId)

    if (!user) {
      throw unauthorizedError()
    }

    req.user = user

    next()
  }

  async create (req: express.Request, res: express.Response) {
    const email = req.body.email
    const password = req.body.password

    if (
      !this.validationsService.isNotEmptyText(email) ||
      !this.validationsService.isNotEmptyText(password) ||
      !this.validationsService.email(email)
    ) {
      throw parametersError()
    }

    const user = await this.usersService.getByEmailAndPassword(email, password)

    if (!user) {
      throw parametersError()
    }

    const token = await this.sessionsService.create(user)

    res.status(201).send(token)
  }
}
