import express from 'express'
import { CalendarsService, FollowersService, ValidationsService } from '../services'
import { conflictError, forbiddenError, internalServerError, notFoundError, parametersError } from '../utils'

export class FollowersController {
  private static instance: FollowersController | null;
  private validationsService: ValidationsService;
  private followersService: FollowersService;
  private calendarsService: CalendarsService;

  private constructor (
    validationsService: ValidationsService,
    FollowersService: FollowersService,
    calendarsService: CalendarsService
  ) {
    this.validationsService = validationsService
    this.followersService = FollowersService
    this.calendarsService = calendarsService
  }

  static getInstance (
    validationsService?: ValidationsService,
    FollowersService?: FollowersService,
    calendarsService?: CalendarsService
  ) {
    if (!FollowersController.instance) {
      if (!validationsService || !FollowersService || !calendarsService) {
        throw new Error('[FollowersController] Init error')
      }

      FollowersController.instance = new FollowersController(validationsService, FollowersService, calendarsService)
    }

    return FollowersController.instance
  }

  async getByCalendarId (req: express.Request, res: express.Response) {
    const user = req.user
    const calendarId = req.params.calendarId || ''

    if (!user) {
      throw parametersError()
    }

    if (!this.validationsService.isUUID(calendarId)) {
      throw parametersError()
    }

    const calendar = await this.calendarsService.getByCalendarId(calendarId)

    if (!calendar) {
      throw notFoundError()
    }

    const followers = await this.followersService.getByCalendarId(calendar)

    if (!followers.find((follower) => follower.userId === user.userId)) {
      throw forbiddenError()
    }

    res.status(200).send(followers)
  }

  async create (req: express.Request, res: express.Response) {
    const user = req.user
    const calendarId = req.body.calendarId

    if (!user) {
      throw parametersError()
    }

    if (!this.validationsService.isUUID(calendarId)) {
      throw parametersError()
    }

    const calendar = await this.calendarsService.getByCalendarId(calendarId)

    if (!calendar) {
      throw parametersError()
    }

    const existingFollower = await this.followersService.getByUserAndCalendar(user, calendar)

    if (existingFollower) {
      throw conflictError()
    }

    const follower = await this.followersService.create(user, calendar)

    await this.followersService.notifyFollowerCreated(calendar, user)

    res.status(201).send(follower)
  }

  async remove (req: express.Request, res: express.Response) {
    const user = req.user
    const followerId = String(req.body.followerId)

    if (!user) {
      throw parametersError()
    }

    if (!this.validationsService.isUUID(followerId)) {
      throw parametersError()
    }

    const existingFollower = await this.followersService.getByFollowerId(followerId)

    if (!existingFollower) {
      throw notFoundError()
    }

    const calendar = await this.calendarsService.getByCalendarId(existingFollower.calendarId)

    if (!calendar) {
      throw internalServerError()
    }

    await this.followersService.notifyFollowerRemoved(calendar, user)
    await this.followersService.remove(followerId)

    res.status(204).send()
  }
}
