import express from 'express'
import { constants } from '../config'
import { CalendarsService, ValidationsService } from '../services'
import { CalendarVisibility } from '../typings'
import { forbiddenError, internalServerError, notFoundError, parametersError } from '../utils'

export class CalendarsController {
  private static instance: CalendarsController | null;
  private validationsService: ValidationsService;
  private calendarsService: CalendarsService;

  private constructor (validationsService: ValidationsService, calendarsService: CalendarsService) {
    this.validationsService = validationsService
    this.calendarsService = calendarsService
  }

  static getInstance (validationsService?: ValidationsService, calendarsService?: CalendarsService) {
    if (!CalendarsController.instance) {
      if (!validationsService || !calendarsService) {
        throw new Error('[CalendarsController] Init error')
      }

      CalendarsController.instance = new CalendarsController(validationsService, calendarsService)
    }

    return CalendarsController.instance
  }

  async create (req: express.Request, res: express.Response) {
    const user = req.user
    const name = req.body.name
    const description = req.body.description
    const visibility = req.body.visibility

    if (!user) {
      throw parametersError()
    }

    if (
      !this.validationsService.isNotEmptyText(name) ||
      !this.validationsService.isNotEmptyText(description) ||
      !this.validationsService.isNotEmptyText(visibility) ||
      !visibility
    ) {
      throw parametersError()
    }

    const calendar = await this.calendarsService.create(user, name, description, visibility)

    res.status(201).send(calendar)
  }

  async list (req: express.Request, res: express.Response) {
    const requestedPublicOnes = req.query.public === 'true'

    if (requestedPublicOnes) {
      return this.listPublic(req, res)
    } else {
      return this.listPrivate(req, res)
    }
  }

  private async listPrivate (req: express.Request, res: express.Response) {
    const user = req.user
    const skip = Number(req.query.skip || 0)
    const take = Number(req.query.take || constants.DEFAULT_PAGE_SIZE)

    if (!user) {
      throw parametersError()
    }

    if (!this.validationsService.isValidNumber(skip) || !this.validationsService.isValidNumber(take)) {
      throw parametersError()
    }

    const calendars = await this.calendarsService.getPrivate(user, skip, take)

    res.status(200).send(calendars)
  }

  private async listPublic (req: express.Request, res: express.Response) {
    const user = req.user
    const skip = Number(req.query.skip || 0)
    const take = Number(req.query.take || constants.DEFAULT_PAGE_SIZE)

    if (!user) {
      throw parametersError()
    }

    if (!this.validationsService.isValidNumber(skip) || !this.validationsService.isValidNumber(take)) {
      throw parametersError()
    }

    const calendars = await this.calendarsService.getPublic(skip, take)

    res.status(200).send(calendars)
  }

  async editCalendar (req: express.Request, res: express.Response) {
    const user = req.user
    const calendarId = req.params.calendarId || ''
    const name = req.body.name || ''
    const description = req.body.description || ''
    const visibility = req.body.visibility || ''

    if (
      !this.validationsService.isUUID(calendarId) ||
      (this.validationsService.isNotEmptyText(visibility) &&
        ![CalendarVisibility.Private, CalendarVisibility.Public].includes(visibility))
    ) {
      throw parametersError()
    }

    const calendar = await this.calendarsService.getByCalendarId(calendarId)

    if (!calendar) {
      throw notFoundError()
    }

    if (calendar.ownerUserId !== user?.userId) {
      throw forbiddenError()
    }

    await this.calendarsService.edit(calendarId, name, description, visibility)

    const updatedCalendar = await this.calendarsService.getByCalendarId(calendarId)

    if (!updatedCalendar) {
      throw internalServerError()
    }

    await this.calendarsService.notifyCalendarUpdated(calendar, updatedCalendar, user)

    res.status(200).send(updatedCalendar)
  }

  async removeCalendar (req: express.Request, res: express.Response) {
    const user = req.user
    const calendarId = req.params.calendarId || ''

    if (!this.validationsService.isUUID(calendarId)) {
      throw parametersError()
    }

    const calendar = await this.calendarsService.getByCalendarId(calendarId)

    if (!calendar) {
      throw notFoundError()
    }

    if (calendar.ownerUserId !== user?.userId) {
      throw forbiddenError()
    }

    await this.calendarsService.notifyCalendarDeleted(calendar, user)
    await this.calendarsService.remove(calendarId)

    res.status(204).send()
  }
}
