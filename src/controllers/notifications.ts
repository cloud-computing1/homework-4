import express from 'express'
import { NotificationsService, ValidationsService } from '../services'
import { forbiddenError, parametersError } from '../utils'

export class NotificationsController {
  private static instance: NotificationsController | null;
  private validationsService: ValidationsService;
  private notificationsService: NotificationsService;

  private constructor (validationsService: ValidationsService, notificationsService: NotificationsService) {
    this.validationsService = validationsService
    this.notificationsService = notificationsService
  }

  static getInstance (validationsService?: ValidationsService, notificationsService?: NotificationsService) {
    if (!NotificationsController.instance) {
      if (!validationsService || !notificationsService) {
        throw new Error('[NotificationsController] Init error')
      }

      NotificationsController.instance = new NotificationsController(validationsService, notificationsService)
    }

    return NotificationsController.instance
  }

  async subscribe (req: express.Request, res: express.Response) {
    const user = req.user
    const userId = req.body.userId
    const token = req.body.token

    if (!user) {
      throw forbiddenError()
    }

    if (!this.validationsService.isNotEmptyText(userId) || !this.validationsService.isNotEmptyText(token)) {
      throw parametersError()
    }

    if (user?.userId !== userId) {
      throw forbiddenError()
    }

    await this.notificationsService.subscribe(user, token)

    res.status(204).send()
  }
}
