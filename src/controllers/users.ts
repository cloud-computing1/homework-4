import express from 'express'
import { UsersService, ValidationsService } from '../services'
import {
  conflictError,
  forbiddenError,
  internalServerError,
  notFoundError,
  notImplementedError,
  parametersError
} from '../utils'

export class UsersController {
  private static instance: UsersController | null;
  private validationsService: ValidationsService;
  private usersService: UsersService;

  private constructor (validationsService: ValidationsService, usersService: UsersService) {
    this.validationsService = validationsService
    this.usersService = usersService
  }

  static getInstance (validationsService?: ValidationsService, usersService?: UsersService) {
    if (!UsersController.instance) {
      if (!validationsService || !usersService) {
        throw new Error('[UsersController] Init error')
      }

      UsersController.instance = new UsersController(validationsService, usersService)
    }

    return UsersController.instance
  }

  async list () {
    throw notImplementedError()
  }

  async create (req: express.Request, res: express.Response) {
    const email = String(req.body.email)
    const password = String(req.body.password)
    const firstName = String(req.body.firstName)
    const lastName = String(req.body.lastName)

    if (!this.validationsService.email(email) || !this.validationsService.password(password)) {
      throw parametersError()
    }

    const existingUser = await this.usersService.getByEmail(email)

    if (existingUser) {
      throw conflictError()
    }

    const user = await this.usersService.create(email, password, firstName, lastName)

    res.status(201).send(user)
  }

  async getByUserId (req: express.Request, res: express.Response) {
    const userId = req.params.userId || ''

    if (!this.validationsService.isNotEmptyText(userId) || !this.validationsService.isUUID(userId)) {
      throw parametersError()
    }

    const user = await this.usersService.getByUserId(userId)

    if (!user) {
      throw notFoundError()
    }

    res.status(200).send(user)
  }

  async editUser (req: express.Request, res: express.Response) {
    const user = req.user
    const userId = req.params.userId || ''
    const email = req.body.email || ''
    const password = req.body.password || ''
    const firstName = req.body.firstName || ''
    const lastName = req.body.lastName || ''

    if (
      !this.validationsService.isUUID(userId) ||
      (this.validationsService.isNotEmptyText(email) && !this.validationsService.email(email)) ||
      (this.validationsService.isNotEmptyText(password) && !this.validationsService.password(password))
    ) {
      throw parametersError()
    }

    if (user?.userId !== userId) {
      throw forbiddenError()
    }

    await this.usersService.edit(userId, email, password, firstName, lastName)

    const editedUser = await this.usersService.getByUserId(userId)

    if (!editedUser) {
      throw internalServerError()
    }

    res.status(200).send(editedUser)
  }

  async removeUser (req: express.Request, res: express.Response) {
    const user = req.user
    const userId = req.params.userId || ''

    if (!this.validationsService.isUUID(userId)) {
      throw parametersError()
    }

    if (user?.userId !== userId) {
      throw forbiddenError()
    }

    await this.usersService.remove(userId)

    res.status(204).send()
  }

  async resetPassword (req: express.Request, res: express.Response) {
    const email = req.body.email || ''

    if (!this.validationsService.isNotEmptyText(email)) {
      throw parametersError()
    }

    const user = await this.usersService.getByEmail(email)

    if (!user) {
      throw parametersError()
    }

    await this.usersService.resetPassword(email)

    res.status(204).send()
  }
}
