import { HTTPErrorResponse } from '../typings'

export class HTTPError extends Error {
  private code: number;

  constructor (code: number, message: string) {
    super(message)
    this.code = code
  }

  print (): HTTPErrorResponse {
    return {
      code: this.code,
      message: this.message
    }
  }

  get isHTTPError () {
    return true
  }
}

export const isHTTPError = (err: Error | HTTPError): err is HTTPError => (err as HTTPError).isHTTPError === true

export const parametersError = () => new HTTPError(400, 'Invalid parameters')
export const unauthorizedError = () => new HTTPError(401, 'Unauthorized')
export const forbiddenError = () => new HTTPError(403, 'Forbidden')
export const notFoundError = () => new HTTPError(404, 'Not found')
export const methodNotSupportedError = () => new HTTPError(405, 'Method not supported')
export const conflictError = () => new HTTPError(409, 'Conflict')
export const internalServerError = () => new HTTPError(500, 'Internal server error')
export const notImplementedError = () => new HTTPError(501, 'Not implemented (yet?)')
