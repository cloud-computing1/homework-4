import { ValueTransformer } from 'typeorm'
import { regex } from '../config'

export const dashRemover: ValueTransformer = {
  from: (str: string | null | undefined) => {
    if (!str) {
      return str
    } else {
      return str.replace(regex.DASHES, '')
    }
  },
  to: (str: string | null | undefined) => {
    if (!str) {
      return str
    } else if (str.charAt(8) === '-' && str.charAt(13) === '-' && str.charAt(18) === '-' && str.charAt(23) === '-') {
      return str
    } else {
      return `${str.substring(0, 8)}-${str.substring(8, 12)}-${str.substring(12, 16)}-${str.substring(
        16,
        20
      )}-${str.substring(20, 32)}`
    }
  }
}
