export const enlist = (strings: string[]) =>
  strings.length === 0
    ? []
    : strings.reduce(
      (result, string, index, array) => `${result}${index !== array.length - 1 ? ', ' : ' and '}${string}`
    )
