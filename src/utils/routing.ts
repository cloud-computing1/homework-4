import express from 'express'
import expressServeStaticCore from 'express-serve-static-core'
import { Method, Middleware, Verb } from '../typings'

export function asyncRouter (app: express.Router) {
  const verbArray: Verb[] = ['get', 'post', 'put', 'patch', 'delete', 'head', 'options', 'all']

  return verbArray.reduce(
    (result, method) => ({ ...result, [method]: handleVerb(app, method) }),
    {} as Record<Verb, Method>
  )
}

function handleVerb (app: express.Router, verb: Verb) {
  return (path: expressServeStaticCore.PathParams, ...middlewares: Middleware[]) => {
    const newMiddlewares = middlewares.map(handleMiddleware)

    app[verb](path, ...newMiddlewares)
  }
}

function handleMiddleware (middleware: Middleware) {
  return async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
    try {
      await middleware(req, res, next)
    } catch (error) {
      next(error)
    }
  }
}
