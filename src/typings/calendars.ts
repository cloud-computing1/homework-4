/* eslint-disable no-unused-vars */

import { PublicEntity } from './'

export enum CalendarVisibility {
  Public = 'public',
  Private = 'private',
}

export type PublicCalendar = PublicEntity & {
  calendarId: string;
  ownerUserId: string;
  name: string;
  description: string;
  visibility: CalendarVisibility;
  createDate: Date;
  updateDate: Date;
};
