import { BaseEntity } from 'typeorm'
import { PublicEntity } from './'

export interface Service {
  map: <TBaseEntity extends BaseEntity, TPublicEntity extends PublicEntity>(entity: TBaseEntity) => TPublicEntity;
}
