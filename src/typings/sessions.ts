export type PublicSession = {
  userId: string;
  session: string;
};
