/* eslint-disable no-unused-vars */

import { PublicEntity } from './'

export type PublicUser = PublicEntity & {
  userId: string;
  email: string;
  firstName: string;
  lastName: string;
  createDate: Date;
  updateDate: Date;
};
