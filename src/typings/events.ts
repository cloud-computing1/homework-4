/* eslint-disable no-unused-vars */

import { PublicEntity } from './'

export type PublicEvent = PublicEntity & {
  eventId: string;
  ownerUserId: string;
  ownerCalendarId: string;
  name: string;
  description: string;
  startDate: Date;
  endDate: Date;
  createDate: Date;
  updateDate: Date;
};
