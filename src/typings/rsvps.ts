/* eslint-disable no-unused-vars */

import { PublicEntity } from './'

export enum RSVPStatus {
  None = 'none',
  Maybe = 'maybe',
  Going = 'going',
  NotGoing = 'notGoing',
}

export type PublicRSVP = PublicEntity & {
  rsvpId: string;
  userId: string;
  eventId: string;
  status: RSVPStatus;
  createDate: Date;
  updateDate: Date;
};
