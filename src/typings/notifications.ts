/* eslint-disable no-unused-vars */
export enum NotificationTypes {
  PeopleFollowingCalendar,
  PeopleFollowingEvent,
  PeopleFollowingCalendarAndEvent,
}
