export type MapPromises<T> = <R>(
  callback: (currentValue: T, index?: number, array?: T[]) => Promise<R>
) => Promise<R[]>;
export type Batch<T> = (count: number) => T[][];

export interface ArrayFunctions<T> {
  mapPromises: MapPromises<T>;
  batch: Batch<T>;
}
