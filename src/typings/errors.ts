export type HTTPErrorResponse = { code: number; message: string };
