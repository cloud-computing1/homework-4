export type PublicEntity = {
  [key: string]: unknown;
};
