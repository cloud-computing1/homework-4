/* eslint-disable no-unused-vars */

import { PublicUser } from './users'

declare module 'express' {
  interface Request {
    user?: PublicUser;
  }
}
