/* eslint-disable no-unused-vars */

import { PublicEntity } from './'

export type PublicFollower = PublicEntity & {
  userId: string;
  calendarId: string;
  createDate: Date;
  updateDate: Date;
};
