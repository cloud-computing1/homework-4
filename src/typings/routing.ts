import express from 'express'

export type Middleware = (
  request: express.Request,
  response: express.Response,
  next: express.NextFunction
) => Promise<void> | void;

export type Method = (path: string, ...args: Middleware[]) => void;

export type Verb = 'get' | 'post' | 'put' | 'patch' | 'delete' | 'head' | 'options' | 'all';
