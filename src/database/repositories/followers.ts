import { constants } from '../../config'
import { Calendar, Follower, User } from '../entities'
import { getConnection, Repository } from 'typeorm'

export class FollowersRepository {
  private static instance: FollowersRepository | null;
  private repository: Repository<Follower>;

  private constructor () {
    this.repository = getConnection(constants.DATABASE.name).getRepository(Follower)
  }

  static getInstance () {
    if (!FollowersRepository.instance) {
      FollowersRepository.instance = new FollowersRepository()
    }

    return FollowersRepository.instance
  }

  async add (userId: User['userId'], calendarId: Calendar['calendarId']) {
    const entity = this.repository.create({ userId, calendarId })
    return this.repository.save(entity)
  }

  async getByFollowerId (followerId: Follower['followerId']) {
    return this.repository.findOne({ followerId })
  }

  async getByUserIdAndCalendarId (userId: Follower['userId'], calendarId: Follower['calendarId']) {
    return this.repository.findOne({ userId, calendarId })
  }

  async getByUserId (userId: Follower['userId']) {
    return this.repository.find({ userId })
  }

  async getByCalendarId (calendarId: Follower['calendarId']) {
    return this.repository.find({ calendarId })
  }

  async remove (followerId: Follower['followerId']) {
    await this.repository.delete({ followerId })
  }
}
