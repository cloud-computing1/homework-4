import { getConnection, Repository } from 'typeorm'
import { constants } from '../../config'
import { Calendar, Event, RSVP, User } from '../entities'

export class EventsRepository {
  private static instance: EventsRepository | null;
  private repository: Repository<Event>;

  private constructor () {
    this.repository = getConnection(constants.DATABASE.name).getRepository(Event)
  }

  static getInstance () {
    if (!EventsRepository.instance) {
      EventsRepository.instance = new EventsRepository()
    }

    return EventsRepository.instance
  }

  async create (
    ownerUserId: User['userId'],
    ownerCalendarId: Calendar['calendarId'],
    name: string,
    description: string,
    startDate: Date,
    endDate?: Date
  ) {
    const entity = this.repository.create({ ownerUserId, ownerCalendarId, name, description, startDate, endDate })
    return this.repository.save(entity)
  }

  getByEventId (eventId: Event['eventId']) {
    return this.repository.findOne({ eventId })
  }

  getOwnedByUserId (userId: User['userId']) {
    return this.repository.find({ ownerUserId: userId })
  }

  getOwnedByCalendarId (calendarId: Calendar['calendarId']) {
    return this.repository.find({ ownerCalendarId: calendarId })
  }

  getByUserId (userId: User['userId'], skip: number, take: number) {
    return this.repository
      .createQueryBuilder('events')
      .innerJoin(RSVP, 'rsvps', 'rsvps.eventId = events.eventId')
      .where('rsvps.userId = :userId', { userId })
      .skip(skip)
      .limit(take)
      .getMany()
  }

  getByUserIdAndCalendarId (userId: User['userId'], calendarId: Calendar['calendarId'], skip: number, take: number) {
    return this.repository
      .createQueryBuilder('events')
      .innerJoin(RSVP, 'rsvps', 'rsvps.eventId = events.eventId')
      .where('rsvps.userId = :userId', { userId })
      .andWhere('events.ownerCalendarId = :calendarId', { calendarId })
      .skip(skip)
      .limit(take)
      .getMany()
  }

  edit (eventId: Event['eventId'], name: string, description: string, startDate: Date, endDate: Date) {
    return this.repository.update({ eventId }, { name, description, startDate, endDate })
  }

  remove (eventId: Event['eventId']) {
    return this.repository.delete({ eventId })
  }
}
