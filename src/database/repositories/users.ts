import { getConnection, Repository } from 'typeorm'
import { constants } from '../../config'
import { User } from '../entities'

export class UsersRepository {
  private static instance: UsersRepository | null;
  private repository: Repository<User>;

  constructor () {
    this.repository = getConnection(constants.DATABASE.name).getRepository(User)
  }

  static getInstance () {
    if (!UsersRepository.instance) {
      UsersRepository.instance = new UsersRepository()
    }

    return UsersRepository.instance
  }

  async create (email: string, password: string, firstName: string, lastName: string) {
    const entity = this.repository.create({ email, password, firstName, lastName })
    return this.repository.save(entity)
  }

  getByUserId (userId: User['userId']) {
    return this.repository.findOne({ userId })
  }

  getByEmailAndPassword (email: User['email'], password: User['password']) {
    return this.repository.findOne({ email, password })
  }

  getByEmail (email: User['email']) {
    return this.repository.findOne({ email })
  }

  edit (userId: User['userId'], email: string, password: string, firstName: string, lastName: string) {
    return this.repository.update(
      { userId },
      {
        email,
        password,
        firstName,
        lastName
      }
    )
  }

  remove (userId: User['userId']) {
    return this.repository.delete({ userId })
  }
}
