import { getConnection, Repository } from 'typeorm'
import { constants } from '../../config'
import { CalendarVisibility } from '../../typings'
import { Calendar, Follower, User } from '../entities'

export class CalendarsRepository {
  private static instance: CalendarsRepository | null;
  private repository: Repository<Calendar>;

  private constructor () {
    this.repository = getConnection(constants.DATABASE.name).getRepository(Calendar)
  }

  static getInstance () {
    if (!CalendarsRepository.instance) {
      CalendarsRepository.instance = new CalendarsRepository()
    }

    return CalendarsRepository.instance
  }

  async create (ownerUserId: User['userId'], name: string, description: string, visibility: CalendarVisibility) {
    const entity = this.repository.create({ ownerUserId, name, description, visibility })
    return this.repository.save(entity)
  }

  getByCalendarId (calendarId: Calendar['calendarId']) {
    return this.repository.findOne({ calendarId })
  }

  getOwnedByUserId (userId: User['userId'], skip: number, take: number) {
    return this.repository.find({ where: { ownerUserId: userId }, skip, take })
  }

  getPublic (skip: number, take: number) {
    return this.repository.find({ where: { visibility: CalendarVisibility.Public }, skip, take })
  }

  getPrivate (userId: User['userId'], skip: number, take: number) {
    return this.repository
      .createQueryBuilder('calendars')
      .innerJoin(Follower, 'followers', 'followers.calendarId = calendars.calendarId')
      .where('followers.userId = :userId', { userId })
      .skip(skip)
      .limit(take)
      .getMany()
  }

  edit (calendarId: Calendar['calendarId'], name: string, description: string, visibility: CalendarVisibility) {
    return this.repository.update(
      { calendarId },
      {
        name,
        description,
        visibility
      }
    )
  }

  remove (calendarId: Calendar['calendarId']) {
    return this.repository.delete({ calendarId })
  }
}
