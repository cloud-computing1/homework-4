import { getConnection, Repository } from 'typeorm'
import { RSVPStatus } from '../../typings'
import { constants } from '../../config'
import { Calendar, Event, RSVP, User } from '../entities'

export class RSVPsRepository {
  private static instance: RSVPsRepository | null;
  private repository: Repository<RSVP>;

  private constructor () {
    this.repository = getConnection(constants.DATABASE.name).getRepository(RSVP)
  }

  static getInstance () {
    if (!RSVPsRepository.instance) {
      RSVPsRepository.instance = new RSVPsRepository()
    }

    return RSVPsRepository.instance
  }

  async create (userId: User['userId'], eventId: Event['eventId'], status: RSVPStatus) {
    const entity = this.repository.create({ userId, eventId, status })
    return this.repository.save(entity)
  }

  getByRsvpId (rsvpId: RSVP['rsvpId']) {
    return this.repository.findOne({ rsvpId })
  }

  getByUserId (userId: User['userId']) {
    return this.repository.find({ userId })
  }

  getByEventId (eventId: Event['eventId']) {
    return this.repository.find({ eventId })
  }

  getByCalendarId (calendarId: Calendar['calendarId']) {
    return this.repository
      .createQueryBuilder('rsvps')
      .innerJoin(Event, 'events', 'events.eventId = rsvps.eventId')
      .where('events.calendarId = :calendarId', { calendarId })
      .getMany()
  }

  getByUserIdAndEventId (userId: User['userId'], eventId: Event['eventId']) {
    return this.repository.findOne({ userId, eventId })
  }

  remove (rsvpId: RSVP['rsvpId']) {
    return this.repository.delete({ rsvpId })
  }

  setStatus (rsvpId: RSVP['rsvpId'], status: RSVPStatus) {
    return this.repository.update({ rsvpId }, { status })
  }
}
