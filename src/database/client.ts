import { constants } from '../config'
import { Connection, createConnection } from 'typeorm'

export class Database {
  private static instance: Database | null;
  private clientPromise: Promise<Connection>;

  private constructor () {
    this.clientPromise = createConnection(constants.DATABASE)
    this.clientPromise.then(client => client.synchronize())
  }

  static getInstance () {
    if (!Database.instance) {
      Database.instance = new Database()
    }

    return Database.instance
  }

  async getClient () {
    return await this.clientPromise
  }
}
