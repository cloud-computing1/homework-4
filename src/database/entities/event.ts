import { constants } from '../../config'
import { BaseEntity, BeforeInsert, BeforeUpdate, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, Unique } from 'typeorm'
import { dashRemover } from '../../utils'
import { Calendar } from './calendar'
import { User } from './user'

@Entity({ name: 'events' })
@Unique(['eventId'])
export class Event extends BaseEntity {
  @PrimaryColumn({
    type: 'uuid',
    generated: 'uuid',
    transformer: dashRemover
  })
  eventId!: string;

  @ManyToOne(() => User, (user) => user.userId, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  @Column({ type: 'uuid', transformer: dashRemover })
  ownerUserId!: string;

  @ManyToOne(() => Calendar, (calendar) => calendar.calendarId, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  @Column({ type: 'uuid', transformer: dashRemover })
  ownerCalendarId!: string;

  @Column()
  name!: string;

  @Column()
  description!: string;

  @Column({ type: 'timestamp' })
  startDate!: Date;

  @Column({ type: 'timestamp' })
  endDate!: Date;

  @Column({ type: 'timestamp' })
  createDate!: Date;

  @Column({ type: 'timestamp' })
  updateDate!: Date;

  @BeforeInsert()
  beforeInsert () {
    const now = new Date()

    this.createDate ||= now
    this.updateDate ||= now

    this.startDate ||= now
    this.endDate ||= new Date(this.startDate.getTime() + constants.DEFAULT_EVENT_TIME)
  }

  @BeforeUpdate()
  beforeUpdate () {
    const now = new Date()

    this.updateDate = now
  }
}
