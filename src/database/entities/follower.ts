import { BaseEntity, BeforeInsert, BeforeUpdate, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, Unique } from 'typeorm'
import { dashRemover } from '../../utils'
import { Calendar } from './calendar'
import { User } from './user'

@Entity({ name: 'followers' })
@Unique(['followerId'])
export class Follower extends BaseEntity {
  @PrimaryColumn({ type: 'uuid', generated: 'uuid', transformer: dashRemover })
  followerId!: string;

  @ManyToOne(() => User, (user) => user.userId, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  @PrimaryColumn({ type: 'uuid', transformer: dashRemover })
  userId!: string;

  @ManyToOne(() => Calendar, (calendar) => calendar.calendarId, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  @PrimaryColumn({ type: 'uuid', transformer: dashRemover })
  calendarId!: string;

  @Column({ type: 'timestamp' })
  createDate!: Date;

  @Column({ type: 'timestamp' })
  updateDate!: Date;

  @BeforeInsert()
  beforeInsert () {
    const now = new Date()

    this.createDate ||= now
    this.updateDate ||= now
  }

  @BeforeUpdate()
  beforeUpdate () {
    const now = new Date()

    this.updateDate = now
  }
}
