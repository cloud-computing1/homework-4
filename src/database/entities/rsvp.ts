import { BaseEntity, BeforeInsert, BeforeUpdate, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, Unique } from 'typeorm'
import { RSVPStatus } from '../../typings'
import { dashRemover } from '../../utils'
import { Event } from './event'
import { User } from './user'

@Entity({ name: 'rsvps' })
@Unique(['rsvpId'])
export class RSVP extends BaseEntity {
  @PrimaryColumn({ type: 'uuid', generated: 'uuid', transformer: dashRemover })
  rsvpId!: string;

  @ManyToOne(() => User, (user) => user.userId, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  @PrimaryColumn({ type: 'uuid', transformer: dashRemover })
  userId!: string;

  @ManyToOne(() => Event, (event) => event.eventId, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  @PrimaryColumn({ type: 'uuid', transformer: dashRemover })
  eventId!: string;

  @Column()
  status!: RSVPStatus;

  @Column({ type: 'timestamp' })
  createDate!: Date;

  @Column({ type: 'timestamp' })
  updateDate!: Date;

  @BeforeInsert()
  beforeInsert () {
    const now = new Date()

    this.createDate ||= now
    this.updateDate ||= now
    this.status ||= RSVPStatus.None
  }

  @BeforeUpdate()
  beforeUpdate () {
    const now = new Date()

    this.updateDate = now
  }
}
