import { BaseEntity, BeforeInsert, BeforeUpdate, Column, Entity, PrimaryColumn, Unique } from 'typeorm'
import { dashRemover } from '../../utils'

@Entity({ name: 'users' })
@Unique(['userId', 'email'])
export class User extends BaseEntity {
  @PrimaryColumn({
    type: 'uuid',
    generated: 'uuid',
    transformer: dashRemover
  })
  userId!: string;

  @Column()
  email!: string;

  @Column()
  password!: string;

  @Column()
  firstName!: string;

  @Column()
  lastName!: string;

  @Column({ type: 'timestamp' })
  createDate!: Date;

  @Column({ type: 'timestamp' })
  updateDate!: Date;

  @BeforeInsert()
  beforeInsert () {
    const now = new Date()

    this.createDate ||= now
    this.updateDate ||= now
  }

  @BeforeUpdate()
  beforeUpdate () {
    const now = new Date()

    this.updateDate = now
  }
}
