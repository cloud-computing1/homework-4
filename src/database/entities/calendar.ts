import { BaseEntity, BeforeInsert, BeforeUpdate, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, Unique } from 'typeorm'
import { CalendarVisibility } from '../../typings'
import { dashRemover } from '../../utils'
import { User } from './user'

@Entity({ name: 'calendars' })
@Unique(['calendarId'])
export class Calendar extends BaseEntity {
  @PrimaryColumn({
    type: 'uuid',
    generated: 'uuid',
    transformer: dashRemover
  })
  calendarId!: string;

  @ManyToOne(() => User, (user) => user.userId, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  @Column({ type: 'uuid', transformer: dashRemover })
  ownerUserId!: string;

  @Column()
  name!: string;

  @Column()
  description!: string;

  @Column()
  visibility!: CalendarVisibility;

  @Column({ type: 'timestamp' })
  createDate!: Date;

  @Column({ type: 'timestamp' })
  updateDate!: Date;

  @BeforeInsert()
  beforeInsert () {
    const now = new Date()

    this.createDate ||= now
    this.updateDate ||= now
    this.visibility ||= CalendarVisibility.Private
  }

  @BeforeUpdate()
  beforeUpdate () {
    const now = new Date()

    this.updateDate = now
  }
}
